import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:state_management/model/product_item.dart';
import 'package:state_management/provider/cart_provider.dart';

class ProductList extends StatefulWidget {
  ProductList({Key? key}) : super(key: key);
  static final String routeName = "/productList";

  @override
  State<ProductList> createState() => _ProductListState();
}

class _ProductListState extends State<ProductList> {
  final List productList = [
    ProductItem(name: "Arroz", price: 10.0),
    ProductItem(name: "Feijão", price: 20.0),
    ProductItem(name: "Carne", price: 30.0),
  ];

  List carrinho = <ProductItem>[];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Segunda Tela"),
        actions: [
          Text(Provider.of<CartProvider>(context).carrinho.length.toString())
        ],
      ),
      body: ListView.builder(
          itemCount: productList.length,
          itemBuilder: (context, index) => ListTile(
                title: Text(productList.elementAt(index).name),
                subtitle: Text(productList.elementAt(index).price.toString()),
                trailing: ElevatedButton(
                  onPressed: () =>
                      Provider.of<CartProvider>(context, listen: false)
                          .adicionarItem(productList.elementAt(index)),
                  child: Text("Comprar"),
                ),
              )),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.close),
        onPressed: () => Navigator.of(context).pop(),
      ),
    );
  }
}
