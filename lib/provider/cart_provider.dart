import 'package:flutter/material.dart';
import 'package:state_management/model/product_item.dart';

class CartProvider with ChangeNotifier {
  List _carrinho = <ProductItem>[];

  List get carrinho => _carrinho;

  set carrinho(List value) {
    _carrinho = value;
  }

  void adicionarItem(ProductItem item) {
    _carrinho.add(item);
    notifyListeners();
  }
}
