class ProductItem {
  final String name;
  final double price;

  ProductItem({required this.name, required this.price});
}