import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:state_management/provider/cart_provider.dart';
import 'package:state_management/screens/my_home.dart';
import 'package:state_management/screens/product_list.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider<CartProvider>(create: (_) => CartProvider()),
      ],
      child: MaterialApp(
        title: 'Flutter Demo',
        theme: ThemeData(
          primarySwatch: Colors.blue,
        ),
        home: const MyHomePage(title: 'Flutter Demo Home Page'),
        routes: {ProductList.routeName: (context) => ProductList()},
        debugShowCheckedModeBanner: false,
      ),
    );
  }
}
